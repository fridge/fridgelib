package nz.ac.vuw.mcs.fridge;

import java.net.MalformedURLException;
import java.util.Arrays;

import nz.ac.vuw.mcs.fridge.backend.Fridge;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;
import nz.ac.vuw.mcs.fridge.backend.model.AuthenticatedUser;
import nz.ac.vuw.mcs.fridge.backend.model.OrderLine;

public class TestClient {
	public static void main(String[] args) {
		try {
			Fridge fridge = new Fridge("fridge1", "http://localhost/~andrew/fridgeweb/fridgeserver.php");

			AuthenticatedUser user = fridge.authenticateUser("andrew", "");
			System.out.println("Balance for " + user.getRealName() + " is " + Fridge.formatMoney(user.getBalance()));
			System.out.println(user.getRealName() + " is" + (user.isAdmin() ? "" : " not") + " an admin");

			System.out.println(fridge.getStockForUser(user));
			System.out.println("Purchase: " + Fridge.formatMoney(user.purchase(fridge, Arrays.asList(new OrderLine("KK", 2)))));
			System.out.println("Balance for " + user.getRealName() + " is now " + Fridge.formatMoney(user.getBalance()));
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (InterfridgeException e) {
			e.printStackTrace();
		}
	}
}
