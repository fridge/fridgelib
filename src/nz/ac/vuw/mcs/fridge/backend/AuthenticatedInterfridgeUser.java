package nz.ac.vuw.mcs.fridge.backend;

import java.util.List;
import java.util.Vector;

import nz.ac.vuw.mcs.fridge.backend.model.AuthenticatedUser;
import nz.ac.vuw.mcs.fridge.backend.model.FridgeTransaction;
import nz.ac.vuw.mcs.fridge.backend.model.OrderLine;
import nz.ac.vuw.mcs.fridge.backend.model.User;

public class AuthenticatedInterfridgeUser extends InterfridgeUser implements AuthenticatedUser {
	//We need to keep the password around because it is needed to make purchases
	private final String password;
	private int balance;

	public AuthenticatedInterfridgeUser(Fridge fridge, String username, String password, int balance) {
		super(fridge, username);
		this.password = password;
		this.balance = balance;
	}

	public int getBalance() {
		return balance;
	}

	public List<FridgeTransaction> getTransactionFeed() {
		//No transactions for interfridge users
		return new Vector<FridgeTransaction>();
	}

	public int purchase(Fridge purchaseFridge, List<OrderLine> order) throws InterfridgeException {
		String snonce = fridge.callGenerateNonce(username, password);
		Fridge.PurchaseResult result;
		if (purchaseFridge.equals(fridge)) {
			result = fridge.callPurchase(snonce, username, order, password);
		}
		else {
			result = fridge.callRemotePurchase(snonce, purchaseFridge.getName(), username, order, password);
		}
		balance = result.balance;
		return result.orderTotal;
	}

	public void topup(Fridge topupFridge, int amount) throws InterfridgeException {
		String snonce = fridge.callGenerateNonce(username, password);
		if (topupFridge.equals(fridge)) {
			balance = fridge.callTopup(snonce, username, amount, password);
		}
		else {
			balance = fridge.callRemoteTopup(snonce, topupFridge.getName(), username, amount, password);
		}
	}

	public void transfer(User toUser, int amount) throws InterfridgeException {
		String snonce = fridge.callGenerateNonce(username, password);
		String toUsername;
		if (toUser.getFridge().equals(fridge)) {
			//Local user
			toUsername = toUser.getLocalUsername();
		}
		else {
			toUsername = toUser.getUsername();
		}
		balance = fridge.callTransfer(snonce, username, toUsername, amount, password);
	}
}
