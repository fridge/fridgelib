package nz.ac.vuw.mcs.fridge.backend.util;

import java.text.DecimalFormat;

/**
 * Class to be used statically to format any double into a currency string
 * @author neil
 *
 */
public class MoneyHelper {
	private static DecimalFormat currencyFormatter = new DecimalFormat("$###,##0.00");
	/**
	 * Formats a double into a dollar-currency string, with comma thousands separator and dollar sign
	 * @params input Is the double number to format
	 * @returns a string of $xxx,xxx.xx format
	 */ 
	public static String formatCurrency(double input) {
		return currencyFormatter.format(input);
	}
	
	public static double getPriceWithMarkup(double cost, double markup) {
		return round2dp( cost + (cost * (markup/100D)) );
		
	}
	
	public static double round2dp(double in){
		return Math.round(in*100D)/100D;
	}
}
