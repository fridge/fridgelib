package nz.ac.vuw.mcs.fridge.backend.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Crypt {
	private static final String NONCE_CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final int NONCE_LENGTH = 20;

	public static String hex(byte[] array) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < array.length; ++i) {
			sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).toUpperCase().substring(1,3));
		}
		return sb.toString();
	}

	public static String md5(String message) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			return hex (md.digest(message.getBytes("CP1252"))).toLowerCase();
		}
		catch (NoSuchAlgorithmException e) {
		}
		catch (UnsupportedEncodingException e) {
		}
		return null;
	}

	/**
	 * Given a shared secret and a message, calculate the HMAC-MD5 for the message and convert it the hexadecimal.
	 * @param secret The shared secret.
	 * @param message The message.
	 * @return The HMAC-MD5 in hexadecimal.
	 */
	public static String hmac_md5(String secret, String message) {
		try {
			SecretKey sk = new SecretKeySpec(secret.getBytes("CP1252"), "HmacMD5");
			Mac mac = Mac.getInstance("HmacMD5");
			mac.init(sk);
			byte[] result = mac.doFinal(message.getBytes("CP1252"));
			return hex(result).toLowerCase();
		}
		catch (UnsupportedEncodingException e) {
		}
		catch (NoSuchAlgorithmException e) {
		}
		catch (InvalidKeyException e) {
		}
		return null;
	}

	/**
	 * Create a new random nonce string.
	 * @return
	 */
	public static String makeNonce() {
		char[] nonce = new char[NONCE_LENGTH];
		for (int i = 0; i < NONCE_LENGTH; ++i) {
			nonce[i] = NONCE_CHARS.charAt((int) (Math.random() * NONCE_LENGTH));
		}
		return new String(nonce);
	}

	public static void main(String[] args){
		System.out.println(md5("hello"));
		System.out.println(hmac_md5("secret", "message"));
	}
}
