package nz.ac.vuw.mcs.fridge.backend.xmlrpc;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;

public class XmlRpcClientFactory {
	public static GenericXmlRpcClient makeClient(String endpoint) throws MalformedURLException {
		try {
			Class<? extends GenericXmlRpcClient> androidClass = Class.forName("nz.ac.vuw.mcs.fridge.backend.xmlrpc.AndroidXmlRpcClient").asSubclass(GenericXmlRpcClient.class);
			Constructor<? extends GenericXmlRpcClient> constructor = androidClass.getConstructor(String.class);
			return constructor.newInstance(endpoint);
		}
		catch (ClassNotFoundException e) {
			//Ignore, fall through to next option
		}
		catch (NoSuchMethodException e) {
			throw new RuntimeException("Missing method on AndroidXmlRpcClient. This should never happen.", e);
		}
		catch (InvocationTargetException e) {
			if (e.getCause() instanceof MalformedURLException) {
				throw (MalformedURLException) e.getCause();
			}
		}
		catch (IllegalAccessException e) {
			throw new RuntimeException("Error constructing ApacheXmlRpcClient.", e);
		}
		catch (InstantiationException e) {
			throw new RuntimeException("Error constructing ApacheXmlRpcClient.", e);
		}

		try {
			Class.forName("org.apache.xmlrpc.client.XmlRpcClient"); //This will throw an exception if the class does not exist, thus skipping the following code
			Class<? extends GenericXmlRpcClient> apacheClass = Class.forName("nz.ac.vuw.mcs.fridge.backend.xmlrpc.ApacheXmlRpcClient").asSubclass(GenericXmlRpcClient.class);
			Constructor<? extends GenericXmlRpcClient> constructor = apacheClass.getConstructor(String.class);
			return (GenericXmlRpcClient) constructor.newInstance(endpoint);
		}
		catch (ClassNotFoundException e) {
			//Ignore, fall through to next option
		}
		catch (NoSuchMethodException e) {
			throw new RuntimeException("Missing method on ApacheXmlRpcClient. This should never happen.", e);
		}
		catch (InvocationTargetException e) {
			if (e.getCause() instanceof MalformedURLException) {
				throw (MalformedURLException) e.getCause();
			}
		}
		catch (IllegalAccessException e) {
			throw new RuntimeException("Error constructing ApacheXmlRpcClient.", e);
		}
		catch (InstantiationException e) {
			throw new RuntimeException("Error constructing ApacheXmlRpcClient.", e);
		}

		throw new RuntimeException("No suitable XML RPC client library found.");
	}
}
