package nz.ac.vuw.mcs.fridge.backend.xmlrpc;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class ApacheXmlRpcClient implements GenericXmlRpcClient {
	private final XmlRpcClient client;

	public ApacheXmlRpcClient(String endpoint) throws MalformedURLException {
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
	    config.setServerURL(new URL(endpoint));
	    client = new XmlRpcClient();
	    client.setConfig(config);
	}

	public Object execute(String method, Object[] arguments) throws GenericXmlRpcException {
		try {
			return client.execute(method, arguments);
		}
		catch (XmlRpcException e) {
			throw new GenericXmlRpcException(e);
		}
	}
}
