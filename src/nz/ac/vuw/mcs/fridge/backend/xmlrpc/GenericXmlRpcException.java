package nz.ac.vuw.mcs.fridge.backend.xmlrpc;

public class GenericXmlRpcException extends Exception {
	private static final long serialVersionUID = 1L;

	public GenericXmlRpcException(Throwable e) {
		super(e);
	}

	public String getMessage() {
		return getCause().getMessage();
	}
}
