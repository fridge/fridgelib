package nz.ac.vuw.mcs.fridge.backend.xmlrpc;

public interface GenericXmlRpcClient {
	public Object execute(String method, Object[] arguments) throws GenericXmlRpcException;
}
