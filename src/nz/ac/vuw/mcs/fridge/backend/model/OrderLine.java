/*
 * Created on Mar 26, 2005
 */
package nz.ac.vuw.mcs.fridge.backend.model;

/**
 * @author neil
 */
public class OrderLine {
	private final String productCode;
	private int productQty;

	public OrderLine(String newProductCode, int newProductQty) {
		productCode = newProductCode;
		productQty = newProductQty;
	}

	public String getCode() {
		return productCode;
	}

	public int getQty() {
		return productQty;
	}

	public void incrementQty(int by) {
		productQty += by;
	}

	public void setQty(int quantity) {
		productQty = quantity;
	}
}
