package nz.ac.vuw.mcs.fridge.backend.model;

import java.sql.Date;

public class FridgeTransaction {
	public final Date date;
	public final float amount;
	public final String type;

	public FridgeTransaction(Date date, float amount, String type) {
		this.date = date;
		this.amount = amount;
		this.type = type;
	}
}
