package nz.ac.vuw.mcs.fridge.backend.model;

import nz.ac.vuw.mcs.fridge.backend.Fridge;

/**
 * This class holds information about an item stocked by a fridge.
 * @author andrew
 */
public class StockItem {
	public final String productCode, description, category;
	public final int inStock, price, categoryOrder;

	public StockItem(String productCode, String description, int inStock, int price, String category, int categoryOrder) {
		this.productCode = productCode;
		this.description = description;
		this.inStock = inStock;
		this.price = price;
		this.category = category;
		this.categoryOrder = categoryOrder;
	}

	public String toString() {
		return inStock + "*" + productCode + " (" + description + ")" + "@" + Fridge.formatMoney(price) + " (" + category + ")";
	}

	public ProductCategory getCategory() {
		return new ProductCategory(categoryOrder, category);
	}
}
