package nz.ac.vuw.mcs.fridge.backend;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nz.ac.vuw.mcs.fridge.backend.model.AuthenticatedUser;
import nz.ac.vuw.mcs.fridge.backend.model.OrderLine;
import nz.ac.vuw.mcs.fridge.backend.model.StockItem;
import nz.ac.vuw.mcs.fridge.backend.model.User;
import nz.ac.vuw.mcs.fridge.backend.util.Crypt;
import nz.ac.vuw.mcs.fridge.backend.xmlrpc.GenericXmlRpcClient;
import nz.ac.vuw.mcs.fridge.backend.xmlrpc.GenericXmlRpcException;
import nz.ac.vuw.mcs.fridge.backend.xmlrpc.XmlRpcClientFactory;

/**
 * This class represents a remote fridge to which we connect using the interfridge protocol.
 * @author AndrewWalbran
 */
public class Fridge {
	/**
	 * The name of the fridge.
	 */
	private final String name;

	/**
	 * The HTTP endpoint URL used to connect to the remote fridge.
	 */
	private final String endpoint;

	private final GenericXmlRpcClient xmlrpc;

	/**
	 * Cached list of remote fridges known to this fridge.
	 * This is a map of fridge name to endpoint URL.
	 */
	private Map<String, Fridge> fridges;

	/**
	 * Number of people served, cached from calling get_messages.
	 */
	private int numServed;
	
	/**
	 * Message about how to sign up for fridge, cached from calling get_messages.
	 */
	private String signupInfo;

	/**
	 * List of people who owe fridge money and should be told off, cached from calling get_messages.
	 */
	private List<String> naughtyPeople;

	public Fridge(String name, String endpoint) throws MalformedURLException {
		if (name == null || endpoint == null) {
			throw new NullPointerException();
		}
		this.name = name;
		this.endpoint = endpoint;

		xmlrpc = XmlRpcClientFactory.makeClient(endpoint);
	}

	public Fridge(String endpoint) throws MalformedURLException, InterfridgeException {
		if (endpoint == null) {
			throw new NullPointerException();
		}
		this.endpoint = endpoint;
		xmlrpc = XmlRpcClientFactory.makeClient(endpoint);
		this.name = callGetFridgeName();
	}

	/**
	 * Authenticate a user at this fridge with the given username and password. If the username and password provided are valid then return the user; else throw an InterfridgeException.
	 * @param username The username of the user at this fridge to authenticate.
	 * @param password Their password.
	 * @return An object representing the user.
	 * @throws InterfridgeException If there is an error communicating with the fridge server or authenticating the user.
	 */
	public AuthenticatedUser authenticateUser(String username, String password) throws InterfridgeException {
		String snonce = callGenerateNonce(username, password);
		int balance = callTransfer(snonce, username, username, 0, password);
		return new AuthenticatedInterfridgeUser(this, username, password, balance);
	}

	/**
	 * Get an object representing a user at this fridge with the given username.
	 * @param username The username of the user at this fridge.
	 * @return An object representing the user.
	 * @throws InterfridgeException If there is an error communicating with the fridge server or the user does not exist.
	 */
	public User getUser(String username) throws InterfridgeException {
		User user = new InterfridgeUser(this, username);
		user.getRealName(); //Ignore the result, we are just calling this to force the object to initialise itself by calling a method on the fridge, to verify that they actually exist
		return user;
	}

	/**
	 * Get a local or remote user, by either a local username or username@fridge.
	 * @param identifier A string identifying the local or remote user, including the fridge name if appropriate.
	 * @return An object representing the user.
	 * @throws InterfridgeException
	 * @throws IllegalArgumentException If an invalid remote fridge name is given.
	 */
	public User parseUser(String identifier) throws InterfridgeException, IllegalArgumentException {
		int at = identifier.indexOf('@');
		if (at == -1) {
			//Local user
			return getUser(identifier);
		}
		else {
			String fridgename = identifier.substring(at + 1);
			String username = identifier.substring(0, at);
			if (fridgename.equals(name)) {
				//Fully-qualified local user
				return getUser(username);
			}
			Fridge fridge = getRemoteFridge(fridgename);
			if (fridge == null) {
				//No such fridge
				throw new IllegalArgumentException("No such fridge '" + identifier.substring(at + 1) + "'");
			}
			return fridge.getUser(username);
		}
	}

	/**
	 * Authenticate a local or remote user, by either a local username or username@fridge.
	 * @param identifier A string identifying the local or remote user, including the fridge name if appropriate.
	 * @param password The user's password.
	 * @return An object representing the authenticated user.
	 * @throws InterfridgeException
	 * @throws IllegalArgumentException If an invalid remote fridge name is given.
	 */
	public AuthenticatedUser parseAuthenticateUser(String identifier, String password) throws InterfridgeException, IllegalArgumentException {
		int at = identifier.indexOf('@');
		if (at == -1) {
			//Local user
			return authenticateUser(identifier, password);
		}
		else {
			String fridgename = identifier.substring(at + 1);
			String username = identifier.substring(0, at);
			if (fridgename.equals(name)) {
				//Fully-qualified local user
				return authenticateUser(username, password);
			}
			Fridge fridge = getRemoteFridge(fridgename);
			if (fridge == null) {
				//No such fridge
				throw new IllegalArgumentException("No such fridge '" + identifier.substring(at + 1) + "'");
			}
			return fridge.authenticateUser(username, password);
		}
	}

	/**
	 * Get the unique name of this fridge. This name is also used as the username for its interfridge account on all other fridges.
	 * @return The name of the fridge.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the number of servings made by this fridge, to brag about it.
	 * @return How many purchases have been made on the fridge.
	 * @throws InterfridgeException
	 */
	public int getNumServed() throws InterfridgeException {
		ensureGotMessages();
		return numServed;
	}

	/**
	 * Get a message about how to signup for a fridge account at this fridge.
	 * @return Said message.
	 * @throws InterfridgeException
	 */
	public String getSignupInfo() throws InterfridgeException {
		ensureGotMessages();
		return signupInfo;
	}

	/**
	 * Get a list of people who owe the fridge money, and should be told off.
	 * @return A list of these people's names.
	 * @throws InterfridgeException
	 */
	public List<String> getNaughtyPeople() throws InterfridgeException {
		ensureGotMessages();
		return naughtyPeople;
	}

	/**
	 * Call get_messages() to initialise a few fields with messages and statistics.
	 */
	private void ensureGotMessages() throws InterfridgeException {
		if (signupInfo == null) {
			refreshMessages();
		}
	}

	/**
	 * Call get_messages to refresh the value that will be returned by getNumServed(), getSignupInfo() and getNaughtyPeople().
	 * This will be called automatically the first time one of those methods is called, but it may be desirable to call it manually at some later point to update the cached values.
	 */
	public void refreshMessages() throws InterfridgeException {
		Map<String, Object> messages = callGetMessages();
		numServed = (Integer) messages.get("num_served");
		signupInfo = (String) messages.get("signup_info");

		Object[] naughtyArray = (Object[]) messages.get("naughty_people");
		naughtyPeople = new ArrayList<String>(naughtyArray.length);
		for (Object el: naughtyArray) {
			naughtyPeople.add((String) el);
		}
	}

	/**
	 * Initialise the fridges field if it has not already been initialised.
	 */
	private void getFridges() throws InterfridgeException {
		if (fridges == null) {
			Map<String, String> fridgeEndpoints = callGetFridges();
			fridges = new HashMap<String, Fridge>();
			for (Map.Entry<String, String> fridge: fridgeEndpoints.entrySet()) {
				try {
					fridges.put(fridge.getKey(), new Fridge(fridge.getKey(), fridge.getValue()));
				}
				catch (MalformedURLException e) {
					throw new InterfridgeException("Invalid URL returned for fridge '" + fridge.getKey() + "'", e);
				}
			}
		}
	}

	/**
	 * Given the name of a remote fridge which this fridge knows about, construct and return the appropriate Fridge.
	 * @param name The name of the remote fridge.
	 * @return A Fridge object representing the requested remote fridge, or null if no such fridge is known.
	 * @throws InterfridgeException
	 */
	public Fridge getRemoteFridge(String name) throws InterfridgeException {
		getFridges();
		return fridges.get(name);
	}

	/**
	 * Get a list of remote fridge names.
	 * @return A list of the names of all the remote fridges peered with this fridge.
	 * @throws InterfridgeException
	 */
	public Set<String> getRemoteFridges() throws InterfridgeException {
		getFridges();
		return fridges.keySet();
	}

	private Map<String, String> callGetFridges() throws InterfridgeException {
		try {
			Object ret = xmlrpc.execute("get_fridges", new Object[]{});
			if (ret instanceof Map) {
				return (Map<String, String>) ret;
			}
			else {
				return new HashMap<String, String>();
			}
		}
		catch (GenericXmlRpcException e) {
			throw new InterfridgeException("Error calling get_fridges method", e);
		}
	}

	/**
	 * Call the generate_nonce method on the fridge server to fetch a nonce for the following request.
	 * @return The nonce provided by the fridge.
	 */
	protected String callGenerateNonce(String username, String password) throws InterfridgeException {
		//Generate client nonce and timestamp
		long timestamp = new Date().getTime() / 1000;
		String cnonce = Crypt.makeNonce();
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), cnonce + "," + timestamp + "," + username);

		Map<String, Object> result;
		try {
			result = (Map<String, Object>) xmlrpc.execute("generate_nonce", new Object[]{cnonce, (int) timestamp, username, requestHmac}); //TODO: Work out type for timestamp
		}
		catch (GenericXmlRpcException e) {
			throw new InterfridgeException("Error calling generate_nonce method", e);
		}
		String snonce = (String) result.get("nonce");
		String responseHmac = (String) result.get("hmac");
		if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + cnonce))) {
			throw new InterfridgeException("Invalid HMAC on nonce response.");
		}
		return snonce;
	}

	/**
	 * Call the transfer method on the fridge.
	 * @return The new balance of <tt>fromUser</tt> after the successful transfer.
	 * @throws InterfridgeException If an error occurred communicating with the server, or if the server returned an error.
	 */
	protected int callTransfer(String snonce, String fromUser, String toUser, int amount, String password) throws InterfridgeException {
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), snonce + "," + fromUser + "," + toUser + "," + amount);
		Map<String, Object> result;
		try {
			result = (Map<String, Object>) xmlrpc.execute("transfer", new Object[]{snonce, fromUser, toUser, amount, requestHmac});
		}
		catch (GenericXmlRpcException e) {
			throw new InterfridgeException("Error calling transfer method", e);
		}

		int balance;
		try {
			balance = (Integer) result.get("balance");
		}
		catch (NullPointerException e) {
			throw new InterfridgeException("Balance missing in response from remote fridge to transfer method.");
		}
		String responseHmac = (String) result.get("hmac");
		if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + balance))) {
			throw new InterfridgeException("Invalid HMAC on transfer response.");
		}
		return balance;
	}

	protected int callTopup(String snonce, String username, int amount, String password) throws InterfridgeException {
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), snonce + "," + username + "," + amount);
		Map<String, Object> result;
		try {
			result = (Map<String, Object>) xmlrpc.execute("topup", new Object[]{snonce, username, amount, requestHmac});
		}
		catch (GenericXmlRpcException e) {
			throw new InterfridgeException("Error calling topup method", e);
		}

		int balance;
		try {
			balance = (Integer) result.get("balance");
		}
		catch (NullPointerException e) {
			throw new InterfridgeException("Balance missing in response from fridge to topup method.");
		}
		String responseHmac = (String) result.get("hmac");
		if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + balance))) {
			throw new InterfridgeException("Invalid HMAC on topup response.");
		}
		return balance;
	}

	protected int callRemoteTopup(String snonce, String topupFridge, String username, int amount, String password) throws InterfridgeException {
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), snonce + "," + topupFridge + "," + username + "," + amount);
		Map<String, Object> result;
		try {
			result = (Map<String, Object>) xmlrpc.execute("remote_topup", new Object[]{snonce, topupFridge, username, amount, requestHmac});
		}
		catch (GenericXmlRpcException e) {
			throw new InterfridgeException("Error calling remote_topup method", e);
		}

		int balance;
		try {
			balance = (Integer) result.get("balance");
		}
		catch (NullPointerException e) {
			throw new InterfridgeException("Balance missing in response from fridge to remote_topup method.");
		}
		String responseHmac = (String) result.get("hmac");
		if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + balance))) {
			throw new InterfridgeException("Invalid HMAC on remote_topup response.");
		}
		return balance;
	}

	protected class PurchaseResult {
		public final int balance, orderTotal;

		public PurchaseResult(int balance, int orderTotal) {
			this.balance = balance;
			this.orderTotal = orderTotal;
		}
	}

	protected PurchaseResult callPurchase(String snonce, String username, List<OrderLine> order, String password) throws InterfridgeException {
		Map<String, Object>[] items = new Map[order.size()];
		String itemsString = convertItemsForPurchase(order, items);
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), snonce + "," + username + itemsString);
		Map<String, Object> result;
		try {
			result = (Map<String, Object>) xmlrpc.execute("purchase", new Object[]{snonce, username, items, requestHmac});
		}
		catch (GenericXmlRpcException e) {
			throw new InterfridgeException("Error calling purchase method", e);
		}

		int balance, orderTotal;
		try {
			balance = (Integer) result.get("balance");
			orderTotal = (Integer) result.get("order_total");
		}
		catch (NullPointerException e) {
			throw new InterfridgeException("Balance or order total missing in response from fridge to purchase method.");
		}
		String responseHmac = (String) result.get("hmac");
		if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + balance + "," + orderTotal))) {
			throw new InterfridgeException("Invalid HMAC on purchase response.");
		}
		return new PurchaseResult(balance, orderTotal);
	}

	/**
	 * Helper method used by callPurchase and callRemotePurchase to convert the order into the necessary array and construct the string used in the HMAC.
	 * @param order The order.
	 * @param items An array into which to store the items of the order.
	 * @return A string representing all the items in the order, to be used in calculating the HMAC.
	 */
	private String convertItemsForPurchase(List<OrderLine> order, Map<String, Object>[] items) {
		String itemsString = "";
		int i = 0;
		for (OrderLine line: order) {
			itemsString += "," + line.getCode() + "," + line.getQty();
			items[i] = new HashMap<String, Object>();
			items[i].put("code", line.getCode());
			items[i].put("quantity", line.getQty());
			++i;
		}
		return itemsString;
	}

	protected PurchaseResult callRemotePurchase(String snonce, String purchaseFridge, String username, List<OrderLine> order, String password) throws InterfridgeException {
		Map<String, Object>[] items = new Map[order.size()];
		String itemsString = convertItemsForPurchase(order, items);
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), snonce + "," + purchaseFridge + "," + username + itemsString);
		Map<String, Object> result;
		try {
			result = (Map<String, Object>) xmlrpc.execute("remote_purchase", new Object[]{snonce, purchaseFridge, username, items, requestHmac});
		}
		catch (GenericXmlRpcException e) {
			throw new InterfridgeException("Error calling remote_purchase method", e);
		}

		int balance, orderTotal;
		try {
			balance = (Integer) result.get("balance");
			orderTotal = (Integer) result.get("order_total");
		}
		catch (NullPointerException e) {
			throw new InterfridgeException("Balance or order total missing in response from fridge to remote_purchase method.");
		}
		String responseHmac = (String) result.get("hmac");
		if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + balance + "," + orderTotal))) {
			throw new InterfridgeException("Invalid HMAC on remote_purchase response.");
		}
		return new PurchaseResult(balance, orderTotal);
	}

	protected Map<String, Object> callGetMessages() throws InterfridgeException {
		Map<String, Object> result;
		try {
			result = (Map<String, Object>) xmlrpc.execute("get_messages", new Object[]{});
		}
		catch (GenericXmlRpcException e) {
			throw new InterfridgeException("Error calling get_messages method", e);
		}
		return result;
	}

	protected Map<String, Object> callGetUserInfo(String username) throws InterfridgeException {
		Map<String, Object> result;
		try {
			result = (Map<String, Object>) xmlrpc.execute("get_user_info", new Object[]{username});
		}
		catch (GenericXmlRpcException e) {
			throw new InterfridgeException("Error calling get_user_info method", e);
		}
		return result;
	}

	/**
	 * Get a list of items which this fridge stocks.
	 * @param username If non-null, pass this username on to the fridge so as to get prices specific to that user. If null, give default prices.
	 * @return Said list of stocked items.
	 * @throws InterfridgeException If an error occurred communicating with the server.
	 */
	protected Map<String, StockItem> callGetStock(String username) throws InterfridgeException {
		Object[] result;
		try {
			result = (Object[]) xmlrpc.execute("get_stock", username == null ? new Object[]{} : new Object[]{username});
		}
		catch (GenericXmlRpcException e) {
			throw new InterfridgeException("Error calling get_stock method", e);
		}
		Map<String, StockItem> items = new HashMap<String, StockItem>();
		for (Object itemObj: result) {
			Map<String, Object> item = (Map<String, Object>) itemObj;
			items.put((String) item.get("product_code"), new StockItem((String) item.get("product_code"), (String) item.get("description"), (Integer) item.get("in_stock"), (Integer) item.get("price"), (String) item.get("category"), (Integer) item.get("category_order")));
		}
		return items;
	}

	protected String callGetFridgeName() throws InterfridgeException {
		try {
			return (String) xmlrpc.execute("get_fridge_name", new Object[]{});
		}
		catch (GenericXmlRpcException e) {
			throw new InterfridgeException("Error calling get_fridge_name method", e);
		}
	}

	/**
	 * Get a list of items which this fridge stocks.
	 * @return Said list of stocked items.
	 * @throws InterfridgeException If an error occurred communicating with the server.
	 */
	public Map<String, StockItem> getStock() throws InterfridgeException {
		return callGetStock(null);
	}

	/**
	 * Get a list of items which this fridge stocks, with prices &c. for the given (local or remote) user, taking into account their account type when calculating prices.
	 * If the user is remote then their fridge name is used.
	 * @param user
	 * @return Said list of stocked items.
	 * @throws InterfridgeException If an error occurred communicating with the server.
	 */
	public Map<String, StockItem> getStockForUser(User user) throws InterfridgeException {
		//TODO: Method User.getLocalUsernameForFridge to do this?
		if (user.getFridge().equals(this)) {
			return callGetStock(user.getLocalUsername());
		}
		else {
			return callGetStock(user.getFridge().getName());
		}
	}

	/**
	 * Given an integer amount of money in cents, return a formatted string suitable for display.
	 * @param amount Amount in cents.
	 * @return Formatted price string.
	 */
	public static String formatMoney(int amount) {
		String sign;
		if (amount < 0) {
			amount = -amount;
			sign = "-";
		}
		else {
			sign = "";
		}
		return String.format("%s$%d.%02d", sign, amount / 100, amount % 100);
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Fridge && ((Fridge) other).endpoint.equals(endpoint);
	}
}
