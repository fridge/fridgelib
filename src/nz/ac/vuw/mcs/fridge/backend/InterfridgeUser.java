package nz.ac.vuw.mcs.fridge.backend;

import java.util.Map;

import nz.ac.vuw.mcs.fridge.backend.model.User;

public class InterfridgeUser implements User {
	protected final Fridge fridge;
	protected final String username;

	//These are only initialised when first they are needed. Subclasses should thus use the accessor methods.
	private String realname;
	private Boolean isadmin;

	public InterfridgeUser(Fridge fridge, String username) {
		if (fridge == null) {
			throw new IllegalArgumentException("InterfridgeUser may not have null fridge.");
		}
		else if (username == null) {
			throw new IllegalArgumentException("InterfridgeUser may not have null username.");
		}
		this.fridge = fridge;
		this.username = username;
	}

	public String getUsername() {
		return username + "@" + fridge.getName();
	}

	/**
	 * Helper method to initialise a local cache of various information about this user by calling get_user_info on the fridge server.
	 */
	private void initialiseInfo() throws InterfridgeException {
		Map<String, Object> result = fridge.callGetUserInfo(username);
		try {
			isadmin = (Boolean) result.get("isadmin");
			realname = (String) result.get("real_name");
		}
		catch (ClassCastException e) {
			throw new InterfridgeException("Bad return value from get_user_info", e);
		}
	}

	public String getRealName() throws InterfridgeException {
		if (realname == null) {
			initialiseInfo();
		}
		return realname;
	}

	public boolean isAdmin() throws InterfridgeException {
		if (isadmin == null) {
			initialiseInfo();
		}
		return isadmin;
	}

	public Fridge getFridge() {
		return fridge;
	}

	public String getLocalUsername() {
		return username;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof InterfridgeUser && ((InterfridgeUser) other).fridge.equals(fridge) && ((InterfridgeUser) other).username.equals(username);
	}
}
