package nz.ac.vuw.mcs.fridge.backend;

/**
 * This exception is thrown when an error occurs during an interfridge operation, such as logging in or purchasing.
 * @author andrew
 */
public class InterfridgeException extends Exception {
	private static final long serialVersionUID = 1L;

	public InterfridgeException() {
		super();
	}

	public InterfridgeException(String message, Throwable exception) {
		super(message, exception);
	}

	public InterfridgeException(String message) {
		super(message);
	}

	public InterfridgeException(Throwable exception) {
		super(exception);
	}

	public String getMessage() {
		Throwable cause = getCause();
		if (cause != null) {
			return super.getMessage() + ": " + cause.getMessage();
		}
		else {
			return super.getMessage();
		}
	}
}
